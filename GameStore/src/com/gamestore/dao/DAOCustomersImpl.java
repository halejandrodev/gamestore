package com.gamestore.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.gamestore.connection.Conexion;
import com.gamestore.enumerados.EnumConsultas;
import com.gamestore.interfaces.DAOCustomers;
import com.gamestore.model.CustomersModel;

public class DAOCustomersImpl extends Conexion implements DAOCustomers{

	@Override
	public void registrar(CustomersModel customer) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.POST_CUSTOMER.getValue());
			st.setString(1, customer.getCode());
			st.setString(2, customer.getName());
			st.setString(3, customer.getAddress());
			st.setString(4, customer.getDetails());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
	}

	@Override
	public void modificar(CustomersModel customer) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.PUT_CUSTOMER.getValue());
			st.setString(1, customer.getCode());
			st.setString(2, customer.getName());
			st.setString(3, customer.getAddress());
			st.setString(4, customer.getDetails());
			st.setInt(5, customer.getId());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
	}

	@Override
	public void eliminar(CustomersModel customer) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.DELETE_CUSTOMER.getValue());
			st.setInt(1, customer.getId());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
	}

	@Override
	public List<CustomersModel> listar(String name) throws Exception {
		List<CustomersModel> lista = null;
		try {
			lista = new ArrayList<>();
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.GET_CUSTOMER_BY_ID.getValue());
			st.setString(1, name);
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				CustomersModel customer = new CustomersModel();
				customer.setId(rs.getInt("id"));
				customer.setCode(rs.getString("code"));
				customer.setName(rs.getString("name"));
				customer.setAddress(rs.getString("address"));
				customer.setDetails(rs.getString("details"));
				lista.add(customer);
			}
			rs.close();
			st.close();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		return lista;
	}

	@Override
	public List<CustomersModel> listarAll() throws Exception {
		List<CustomersModel> lista = null;
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.GET_CUSTOMERS.getValue());
			lista = new ArrayList<>();
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				CustomersModel customer = new CustomersModel();
				customer.setId(rs.getInt("id"));
				customer.setCode(rs.getString("code"));
				customer.setName(rs.getString("name"));
				customer.setAddress(rs.getString("address"));
				customer.setDetails(rs.getString("details"));
				lista.add(customer);
			}
			rs.close();
			st.close();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		return lista;
	}

}
