package com.gamestore.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import com.gamestore.connection.Conexion;
import com.gamestore.enumerados.EnumConsultas;
import com.gamestore.interfaces.DAOOrders;
import com.gamestore.interfaces.DAOPurchases;
import com.gamestore.model.OrdersModel;
import com.gamestore.model.PurchasesModel;

public class DAOPurchasesImpl extends Conexion implements DAOPurchases {

	@Override
	public void registrar(PurchasesModel purchase) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.POST_PURCHASE.getValue());
			st.setString(1, purchase.getDate());
			st.setString(2, purchase.getDetails());
			st.setInt(3, purchase.getOrderId());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		
	}

	@Override
	public void modificar(PurchasesModel purchase) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.PUT_PURCHASE.getValue());
			st.setString(1, purchase.getDate());
			st.setString(2, purchase.getDetails());
			st.setInt(3, purchase.getOrderId());
			st.setInt(4, purchase.getId());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		
	}

	@Override
	public void eliminar(PurchasesModel purchase) throws Exception {
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.DELETE_PURCHASE.getValue());
			st.setInt(1, purchase.getId());
			st.executeUpdate();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		
	}

	@Override
	public List<PurchasesModel> listar(int param) throws Exception {
		List<PurchasesModel> lista = null;
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.GET_PURCHASE_BY_CUSTOMER_ID.getValue());
			st.setInt(1, param);
			lista = new ArrayList<>();
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				PurchasesModel purchase = new PurchasesModel();
				purchase.setId(rs.getInt("id"));
				purchase.setOrderDetail(rs.getString("order_detail"));
				purchase.setCustomerName(rs.getString("customer_name"));
				purchase.setDetails(rs.getString("purchase_detail"));
				purchase.setDate(rs.getString("date"));
				lista.add(purchase);
			}
			rs.close();
			st.close();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		return lista;
	}

	@Override
	public List<PurchasesModel> listarAll() throws Exception {
		List<PurchasesModel> lista = null;
		try {
			this.conectar();
			PreparedStatement st = this.conexion.prepareStatement(EnumConsultas.GET_PURCHASES.getValue());
			lista = new ArrayList<>();
			ResultSet rs = st.executeQuery();
			while (rs.next()) {
				PurchasesModel purchase = new PurchasesModel();
				purchase.setId(rs.getInt("id"));
				purchase.setOrderDetail(rs.getString("order_detail"));
				purchase.setCustomerName(rs.getString("customer_name"));
				purchase.setDetails(rs.getString("purchase_detail"));
				purchase.setDate(rs.getString("date"));
				lista.add(purchase);
			}
			rs.close();
			st.close();
		} catch (Exception e) {
			throw e;
		}finally {
			this.cerrar();
		}
		return lista;
	}

}
