package com.gamestore.interfaces;

import java.util.List;

import com.gamestore.model.PurchasesModel;


/*
 * <h1>DAOCustomers</h1>
 * <p>Interface que contiene los metodos principales para la clase purchases</p>
 */
public interface DAOPurchases {
	
	public void registrar(PurchasesModel order) throws Exception;
	public void modificar(PurchasesModel order) throws Exception;
	public void eliminar(PurchasesModel order) throws Exception;
	public List<PurchasesModel> listar(int id) throws Exception;
	public List<PurchasesModel> listarAll() throws Exception;
	
}
