package com.gamestore.interfaces;

import java.util.List;

import com.gamestore.model.OrdersModel;


/*
 * <h1>DAOCustomers</h1>
 * <p>Interface que contiene los metodos principales para la clase orders</p>
 */
public interface DAOOrders {
	
	public void registrar(OrdersModel order) throws Exception;
	public void modificar(OrdersModel order) throws Exception;
	public void eliminar(OrdersModel order) throws Exception;
	public List<OrdersModel> listar(int id) throws Exception;
	public List<OrdersModel> listarAll() throws Exception;
	
}
