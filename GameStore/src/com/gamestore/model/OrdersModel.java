package com.gamestore.model;

import java.util.Date;

/*
 * <h1>Orders<\h1>
 * <p>Clase que contiene los atributos del objecto Orders<\p>
 */
public class OrdersModel {
	
	private int id;
	private String date;
	private String details;
	private int customerId;
	private String customerName;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	public int getCustomerId() {
		return customerId;
	}
	public void setCustomerId(int customerId) {
		this.customerId = customerId;
	}
	public String getCustomerName() {
		return customerName;
	}
	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}
	
	
	
	
	
	

}
